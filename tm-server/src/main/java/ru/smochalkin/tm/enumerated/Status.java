package ru.smochalkin.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.exception.system.StatusNotFoundException;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    @NotNull
    Status(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public static Status getStatus(String value){
        try {
            return Status.valueOf(value);
        } catch (IllegalArgumentException e) {
            throw new StatusNotFoundException();
        }
    }

}
