package ru.smochalkin.tm.dto.result;

public final class Success extends Result {

    public Success() {
        success = true;
        message = "";
    }

}
