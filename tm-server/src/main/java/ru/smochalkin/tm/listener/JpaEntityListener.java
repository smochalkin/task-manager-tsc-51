package ru.smochalkin.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.component.JmsComponent;
import ru.smochalkin.tm.enumerated.EntityOperationType;

import javax.persistence.*;

import static ru.smochalkin.tm.enumerated.EntityOperationType.*;

@NoArgsConstructor
public class JpaEntityListener {

    @PostLoad
    public void postLoad(@NotNull final Object entity) {
        sendMessage(entity, POST_LOAD);
    }

    @PrePersist
    public void prePersist(@NotNull final Object entity) {
        sendMessage(entity, PRE_PERSIST);
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity) {
        sendMessage(entity, POST_PERSIST);
    }

    @PreRemove
    public void preRemove(@NotNull final Object entity) {
        sendMessage(entity, PRE_REMOVE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity) {
        sendMessage(entity, POST_REMOVE);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object entity) {
        sendMessage(entity, PRE_UPDATE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, POST_UPDATE);
    }

    @SneakyThrows
    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        JmsComponent.getInstance().sendMessage(entity, operationType.toString());
    }

}