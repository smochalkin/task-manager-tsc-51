package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.endpoint.ISessionEndpoint;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.result.Fail;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.result.Success;
import ru.smochalkin.tm.dto.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Override
    public SessionDto openSession(@WebParam(name = "login") @NotNull final String login,
                                  @WebParam(name = "password") @NotNull final String password) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @WebMethod
    @Override
    public Result closeSession(@WebParam(name = "session") @NotNull final SessionDto sessionDto) {
        try {
            serviceLocator.getSessionService().close(sessionDto);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
