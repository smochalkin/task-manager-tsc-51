package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.exception.system.StatusNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectUpdateStatusByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-status-update-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Update project status by id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusName = TerminalUtil.nextLine();
        @NotNull final Result result = serviceLocator.getProjectEndpoint().changeProjectStatusById(serviceLocator.getSession(), id, statusName);
        printResult(result);
    }

}
